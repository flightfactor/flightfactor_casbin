package main

import (
	"errors"
	"log"
	"net/http"

	"github.com/casbin/casbin/v2"
	"github.com/golang-jwt/jwt/v5"
	casbin_mw "github.com/labstack/echo-contrib/casbin"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	// "github.com/labstack/echo/v4/middleware"
)

// var (
// 	username = "abc"
// 	password = "123"
// )

func main() {
	e := echo.New()

	// Mediate the access for every request

	// e.GET("/dataset1/*", handleRequest)

	// g := e.Group("/")

	// e.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {

	// 	log.Println("username", username)

	// 	log.Println("password", password)
	// 	// // Be careful to use constant time comparison to prevent timing attacks
	// 	// if subtle.ConstantTimeCompare([]byte(username), []byte("joe")) == 1 &&
	// 	// 	subtle.ConstantTimeCompare([]byte(password), []byte("secret")) == 1 {
	// 	// 	return true, nil
	// 	// }
	// 	return true, nil
	// }))

	e.Use(

		echojwt.WithConfig(echojwt.Config{
			// SigningKey: []byte("secret"),
			SigningKey: []byte("secret"),
			Skipper: func(c echo.Context) bool {
				return c.Request().URL.Path == "/health_check"
			},
		}),
	)

	ce, _ := casbin.NewEnforcer("auth_model.conf", "auth_policy.csv")
	// e.Use(casbin_mw.Middleware(ce))

	e.Use(casbin_mw.MiddlewareWithConfig(casbin_mw.Config{

		Skipper: func(c echo.Context) bool {

			log.Println("Skipper")
			return c.Request().URL.Path == "/health_check"
		},
		UserGetter: func(c echo.Context) (string, error) {
			// username, _, _ := c.Request().BasicAuth()

			token, ok := c.Get("user").(*jwt.Token) // by default token is stored under `user` key
			if !ok {
				return "", c.JSON(http.StatusInternalServerError, errors.New("JWT token missing or invalid"))
			}

			log.Println("User getter", token)

			claims, ok := token.Claims.(jwt.MapClaims) // by default claims is of type `jwt.MapClaims`
			if !ok {
				return "", c.JSON(http.StatusInternalServerError, errors.New("failed to cast claims as jwt.MapClaims"))
			}
			role := claims["role"].(string)

			log.Println("Role getter", role)

			return role, nil
		},

		Enforcer: ce,
	}))

	e.GET("/health_check", healthcheck)
	e.GET("/dataset1/*", handleRequest)

	e.Logger.Fatal(e.Start(":1323"))
}

func handleRequest(c echo.Context) error {

	log.Println("casbin OK")

	return nil

}
func healthcheck(c echo.Context) error {

	log.Println("casbin OK")

	return nil

}
